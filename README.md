# Individual Project 2: rust_actix_web_app

This repository contains a simple web application built with Rust using the Actix web framework. It demonstrates a basic web server that serves static files and implements a simple API endpoint to play a chance game.

## Overview

In this application, I have crafted a "Try Your Luck" game that promises to immerse users in a world of excitement from the moment they begin. The initial interaction is straightforward: a button prompts users to test their fortune. Instantly, users are met with three possible outcomes: a $10 prize, a $5 prize, or no prize at all. If users secure a prize but don't claim the top reward, the game challenges users with a critical decision—pursue greater winnings of $50 or $100 by continuing the game, or exit with their current gains in hand. However, proceeding might result in losing everything. After that, users could choose to clear the results and play the game again. 

## rust_actix_web_app



## Demo Videos
![Video Demo](https://gitlab.com/xx103/individual-project-2/-/wikis/uploads/b3fcec76e4af8c215bd22200eb0561c9/video1519841375.mp4)

## Getting Started

### Prerequisites

- Rust and Cargo (the Rust package manager) installed on your machine.
- Docker (optional, for building and running the application inside a Docker container).
- Revise `main.rs`, `index.html`,`Cargo.toml`

### Running the Application

#### Without Docker

1. **Build the project:**

```shell
cargo build --release
```

2. **Run the application:**

```shell
cargo run --release
```

The server will start, and you can access the web application by navigating to `http://localhost:8080` in your web browser.

#### With Docker

1. **Build the Docker image:**

```shell
docker build -t rust_actix_web_app .
```

2. **Run the Docker container:**

```shell
docker run -p 8080:8080 rust_actix_web_app
```

Again, access the web application at `http://localhost:8080`.

### Project Structure

- `main.rs`: The entry point for the Actix web server.
- `index.html`: The HTML file for the front-end, containing the button and script for the chance game.
- `Cargo.toml` and `Cargo.lock`: Configuration files for Rust dependencies.
- `Dockerfile`: Instructions for building the Docker image.
- `Makefile`: Provides convenient commands for building, running, and cleaning the project.

### Dependencies

- `actix-web`: The web framework for Rust.
- `actix-files`: For serving static files.
- `rand`: For generating random numbers.