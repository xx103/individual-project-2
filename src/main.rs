use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct ContinueGameData {
    current_prize: i32,
}

#[derive(Serialize)]
struct GameResponse {
    message: String,
    current_prize: i32,
}

async fn initial_try_your_luck() -> impl Responder {
    let mut rng = rand::thread_rng();
    let chance: f64 = rng.gen(); // generates a float between 0.0 and 1.0

    let (message, current_prize) = if chance < 0.3 {
        ("You've won $5! Continue or End?", 5)
    } else if chance < 0.7 {
        ("You've won $10! Continue or End?", 10)
    } else {
        ("Thank you. Have a nice day.", 0)
    };

    HttpResponse::Ok().json(GameResponse { message: message.to_string(), current_prize })
}

async fn continue_game(data: web::Json<ContinueGameData>) -> impl Responder {
    let mut rng = rand::thread_rng();
    let chance: f64 = rng.gen();

    let current_prize = data.current_prize;
    let (message, new_prize) = if current_prize > 0 {
        if chance < 0.3 {
            ("You've won $50! Game ends.", 50)
        } else if chance < 0.7 {
            ("You've won $100! Game ends.", 100)
        } else {
            ("Sorry, you've lost everything. Game ends.", 0)
        }
    } else {
        // This branch shouldn't normally be hit due to frontend logic
        ("Invalid state: No initial winnings.", current_prize)
    };

    HttpResponse::Ok().json(GameResponse { message: message.to_string(), current_prize: new_prize })
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(web::resource("/click").route(web::get().to(initial_try_your_luck)))
            .service(web::resource("/continue").route(web::post().to(continue_game)))
            .service(Files::new("/", "./static/").index_file("index.html"))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
